ARG AL_VERSION=edge
FROM registry.alpinelinux.org/img/alpine:${AL_VERSION} as build

RUN apk add --no-cache git go
RUN go install -ldflags '-s -w' honnef.co/go/tools/cmd/staticcheck@2023.1.3
RUN go install -ldflags '-s -w' github.com/kisielk/errcheck@v1.6.3

FROM registry.alpinelinux.org/img/alpine:${AL_VERSION}

RUN adduser -D build -u 1000
RUN apk add --no-cache go redo scdoc

COPY --from=build /root/go/bin/staticcheck /usr/local/bin/staticcheck
COPY --from=build /root/go/bin/errcheck /usr/local/bin/errcheck

# Prevent builds from failing because git is not available
ENV GOFLAGS="-buildvcs=false"

USER build
